<?php

    class calculo
    {
        private $db;
        private $total;
        private $arreglo;

        public function __construct()
        {
            $vec=@$_POST["arreglo"];

            $this->arreglo=$vec;
        }
        
        public function get_calculo()
        {
            $vec=$this->arreglo;
            $vec1=['('];
            $vec2=[')'];
            //$vec=['(','2','x','(','2','+','2',')','+','2',')'];
            //$vec=['(','log','(','2','+','log','(','4',')',')',')'];
            $vec=array_merge($vec1,$vec,$vec2);

     
            $total=0;
            $i=-1;
            $aux=0;
            $vecs=[];

            
            $vec=busca_parentesis($vec,$i);

        
            return $vec;            
        }

        public function get_porcentaje()
        {
            $vec=$this->arreglo;
     
            $i=0;
            $operacion="";

            for($i=0; $i<sizeof($vec); $i++)
            {
                if($vec[$i]==" de ")
                {
                    $operacion=$vec[$i];
                    break;
                }
                else
                {
                    if($vec[$i]==" porcentaje equivalente de ")
                    {
                        $operacion=$vec[$i];
                        break;
                    }
                }
            }

            if(strlen($operacion)>6)
            {
                $calculo=$vec[0]*100/$vec[3];

                return $calculo;
            }
            else
            {
                $calculo=$vec[3];
                $calculo=$calculo*($vec[0]/100);

                return $calculo;
            }

        
            return $vec;            
        }
    }

    
    function busca_parentesis($vec,$j)
    {
        $i=$j+1;
        $vector=sizeof($vec);
        
        
        
        while($i<$vector)
        {
            if($vec[$i]=='(')
            {
                $vec=busca_parentesis($vec,$i);
                $vector=sizeof($vec);            
            }
            else
            {
                if($vec[$i]==')')
                { 
                    $vec=busca_log($vec,$j,$i);               
                    $vec=busca_raiz($vec,$j);
                    $vec=potencia($vec,$j);
                    $vec=mul_div($vec,$j);
                    //$vector=sizeof($vec);
                    $vec=suma_resta($vec,$j);

                    return $vec;
                }
            }
            
            $i++;
        }
        
        return $vec;
    }



    function busca_log($vec, $j, $fin)
    {
        $vec2=[];
        $cont=$j+1;
        $aux=0;

        for($i=$j+1; $i<$fin; $i++)
        {
            if($vec[$i]=='log')
            {
                $aux++;
            }
        }

        if($aux==0)
        {
            return $vec;
        }
        else
        {
            $aux=0;
        }

        for($i=0; $i<=$j; $i++)
        {
            $vec2[$i]=$vec[$i];
        }

        for($i=$j+1; $i<=$fin; $i++)
        {
            if($vec[$i]=='log')
            {
                $vec2[$cont]=log10($vec[$i+1]);
                $cont++;
                $i++;
            }
            else
            {
                $vec2[$cont]=$vec[$i];
                $cont++;
            }
        }

        if(($fin+1) < sizeof($vec))
        {
            for($i=$fin+1; $i<sizeof($vec); $i++)
            {
                $vec2[$cont]=$vec[$i];
                $cont++;
            }
        }

        /*echo("<br>");
        for($i=0; $i<sizeof($vec2); $i++)
        {
            echo($vec2[$i]);
        }
        echo("<br>");*/



        return $vec2;

    }


    function busca_raiz($vec, $j)
    {
        $vec2=[];
        $cont=$j+1;
        $aux=0;

        for($i=$j+1; $i<sizeof($vec); $i++)
        {
            if($vec[$i]==')')
            {
                $fin=$i;
                break;
            }
        }

        for($i=$j+1; $i<$fin; $i++)
        {
            if($vec[$i]=='√')
            {
                $aux++;
            }
        }

        if($aux==0)
        {
            return $vec;
        }
        else
        {
            $aux=0;
        }

        for($i=0; $i<=$j; $i++)
        {
            $vec2[$i]=$vec[$i];
        }

        for($i=$j+1; $i<=$fin; $i++)
        {
            if($vec[$i]=='√')
            {
                $raiz=1;

                for($x=$i+1; $x<$fin; $x++)
                {
                    if($vec[$x]=='√')
                    {
                        $raiz++;
                    }
                    else
                    {
                        $paro=$x;
                        break;
                    }
                }

                for($x=0; $x<$raiz; $x++)
                {
                    if($x==0)
                    {
                        $aux=sqrt($vec[$paro]);
                    }
                    else
                    {
                        $aux=sqrt($aux);
                    }
                }

                $vec2[$cont]=$aux;
                $cont++;

                $i=$paro;
            }
            else
            {
                $vec2[$cont]=$vec[$i];
                $cont++;
            }
        }

        if(($fin+1) < sizeof($vec))
        {
            for($i=$fin+1; $i<sizeof($vec); $i++)
            {
                $vec2[$cont]=$vec[$i];
                $cont++;
            }
        }

        return $vec2;

    }


    function potencia($vec, $j)
    {
        $vec2=[];
        $vecaux=[];
        $total=0;
        $bandera=0;
        $cont=0;
        $aux=0;
        $bandera2=0;

        for($i=0; $i<=$j; $i++)
        {
            $vec2[$i]=$vec[$i];
        }

        for($i=$j+1; $i<sizeof($vec); $i++)
        {
            if($vec[$i]==')')
            {
                $fin=$i;
                break;
            }
        }

        for($i=$j+1; $i<$fin; $i++)
        {
            if($vec[$i]=='^')
            {
                 $bandera++;
            }
        }

        if($bandera==0)
        {
            return $vec;
        }
        else
        {
            $bandera=0;
        }

        $i=0;

        for($i=$j+1; $i<$fin; $i++)
        {
            if($vec[$i]=='^')
            {
                $bandera=0;
                $bandera2++;

                for($x=$i; $x<$fin; $x=$x+2)
                {
                    if($vec[$x]=='^')
                    {
                        if($bandera==0)
                        {
                            $aux=pow($vec[$x-1],$vec[$x+1]);
                            $bandera++;
                            $i=$x;
                        }
                        else
                        {
                            $aux=pow($aux,$vec[$x+1]);
                            $x++;
                            $i=$x;
                        }
                    }
                    else
                    {                        
                        break;
                    }
                }

                $vecaux[$cont]=$aux;
                $cont++;
            }
            else
            {
                if($bandera2==0)
                {
                    if(is_numeric($vec[$i]) && ($vec[$i+1]=='+' || $vec[$i+1]=='-' || $vec[$i+1]=='/' || $vec[$i+1]=='x'))
                    {
                        $vecaux[$cont]=$vec[$i];
                        $cont++;
                        $bandera2++;
                    }
                }
                else
                {
                    if($vec[$i]=='+' || $vec[$i]=='-' || $vec[$i]=='x' || $vec[$i]=='/')
                    {
                        $vecaux[$cont]=$vec[$i];
                        $cont++;
                    }

                    if(is_numeric($vec[$i]))
                    {
                        if(($vec[$i-1]=='+' || $vec[$i-1]=='-' || $vec[$i-1]=='x' || $vec[$i-1]=='/')&&($vec[$i+1]=='+' || $vec[$i+1]=='-' || $vec[$i+1]==')' || $vec[$i+1]=='x' || $vec[$i+1]=='/'))
                        {
                            $vecaux[$cont]=$vec[$i];
                            $cont++;
                        }
                    }
                }
            }
        }


        $cont=$j+1;

        for($i=0; $i<sizeof($vecaux); $i++)
        {
            $vec2[$cont]=$vecaux[$i];
            $cont++;
        }


        if($j==0)
        {
            $vec2[$cont]=$vec[$fin];
        }
        else
        {
            for($i=$fin; $i<sizeof($vec); $i++)
            {
                $vec2[$cont]=$vec[$i];
                $cont++;
            }
        }

        /*echo("<br>pico<br>");
        for($i=0; $i<sizeof($vec2); $i++)
        {
            echo ($vec2[$i]);
        }
        echo("<br>");*/
        
        
        return $vec2;
    }

    
    function mul_div($vec, $j)
    {
        $vec2=[];
        $vecaux=[];
        $total=0;
        $bandera=0;
        $cont=0;
        $aux=0;
        $bandera2=0;

        for($i=0; $i<=$j; $i++)
        {
            //echo($vec[$i]."<br>");
            $vec2[$i]=$vec[$i];
        }

        for($i=$j+1; $i<sizeof($vec); $i++)
        {
            if($vec[$i]==')')
            {
                $fin=$i;
                break;
            }
        }

        for($i=$j+1; $i<$fin; $i++)
        {
            //echo($vec[$i]." ");

            if($vec[$i]=='x' || $vec[$i]=='/')
            {
                 $bandera++;
            }
        }

        //echo("<br>");

        if($bandera==0)
        {
            //echo("pichula");
            return $vec;
        }
        else
        {
            $bandera=0;
        }

        $i=0;

        for($i=$j+1; $i<$fin; $i++)
        {
            if($vec[$i]=='x' || $vec[$i]=='/')
            {
                $bandera=0;
                $bandera2++;

                for($x=$i; $x<$fin; $x=$x+2)
                {
                    if($vec[$x]=='x')
                    {
                        //echo($vec[$x]." - posicion: " . $x . "<br>");
                        if($bandera==0)
                        {
                            $aux=$vec[$x-1]*$vec[$x+1];
                            $bandera++;
                            $i=$x;
                            //echo($aux."<br>");
                        }
                        else
                        {
                            $aux=$aux*$vec[$x+1];
                            //$x++;
                            $i=$x;
                            //echo($aux. "<br>");
                        }
                    }
                    else
                    {
                        if($vec[$x]=='/')
                        {
                            //echo($vec[$x]." - posicion: " . $x . "<br>");
                            if($bandera==0)
                            {
                                $aux=$vec[$x-1]/$vec[$x+1];
                                $bandera++;
                                $i=$x;
                                //echo($aux."<br>");
                            }
                            else
                            {
                                $aux=$aux/$vec[$x+1];
                                $x++;
                                $i=$x;
                                //echo($aux. "<br>");
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                $vecaux[$cont]=$aux;
                $cont++;
            }
            else
            {
                if($bandera2==0)
                {
                    if(is_numeric($vec[$i]) && ($vec[$i+1]=='+' || $vec[$i+1]=='-'))
                    {
                        $vecaux[$cont]=$vec[$i];
                        $cont++;
                        $bandera2++;
                        //echo($vec[$i]." <br>");
                    }
                }
                else
                {
                    if($vec[$i]=='+' || $vec[$i]=='-')
                    {
                        $vecaux[$cont]=$vec[$i];
                        $cont++;
                        //echo($vec[$i]." <br>");
                    }

                    if(is_numeric($vec[$i]))
                    {
                        if(($vec[$i-1]=='+' || $vec[$i-1]=='-')&&($vec[$i+1]=='+' || $vec[$i+1]=='-' || $vec[$i+1]==')'))
                        {
                            $vecaux[$cont]=$vec[$i];
                            $cont++;
                            //echo($vec[$i]." <br>");
                        }
                    }
                }
            }
        }
        
        /*echo("<br>picoxx<br>");
        for($i=0; $i<sizeof($vecaux); $i++)
        {
            echo ($vecaux[$i]);
        }
        echo("<br>".($j+1)."<br>");*/

        $cont=$j+1;

        for($i=0; $i<sizeof($vecaux); $i++)
        {
            $vec2[$cont]=$vecaux[$i];
            $cont++;
        }

        //$vec2=array_merge($vec2,$vecaux); este esta buenisimo

        if($j==0)
        {
            $vec2[$cont]=$vec[$fin];
        }
        else
        {
            for($i=$fin; $i<sizeof($vec); $i++)
            {
                $vec2[$cont]=$vec[$i];
                $cont++;
            }
        }

        /*echo("<br>pico<br>");
        for($i=0; $i<sizeof($vec2); $i++)
        {
            echo ($vec2[$i]);
        }
        echo("<br>");*/
        
        
        return $vec2;
    }

    function suma_resta($vec, $j)
    {
        $total='as';
        $bandera=0;
        $fin=0;
        $vec2=[];

        
        for($i=($j+1); $i<sizeof($vec); $i++)
        {
            if($vec[$i]==')')
            {
                $fin=$i;
                break;
            } 
        }
        
        $elementos=0;

        for($i=$j+1; $i<$fin; $i++)
        {
            $elementos++;
        }       

        if($elementos<=1)
        {
            //echo("error aqui<br>");
        }
        
        for($i=($j+1); $i<$fin; $i++)
        {
            if($bandera==0)
            {
                if($vec[$i]=='+')
                {
                    if(is_numeric($vec[$i-1]))
                    {
                        $total=$vec[$i-1]+$vec[$i+1];
                        $i++;
                        $bandera++;
                    }
                }
                
                if($vec[$i]=='-')
                {
                    if(is_numeric($vec[$i-1]))
                    {
                        $total=$vec[$i-1]-$vec[$i+1];
                        $i++;
                        $bandera++;
                    }
                }
            }
            else
            {
                if($vec[$i]=='+')
                {
                    if(is_numeric($total))
                    {
                        $total=$total+$vec[$i+1];
                        $i++;
                    }
                }
                
                if($vec[$i]=='-')
                {
                    if(is_numeric($total))
                    {
                        $total=$total-$vec[$i+1];
                        $i++;
                    }
                }
            }
        }
        
        if($total=='as')
        {
            $total=$vec[$j+1];
        }
        
        if($j==0)
        {
            $vec2[0]=$total;
            return $vec2;

            /*echo("<br>pico<br>");
            for($i=0; $i<sizeof($vec2); $i++)
            {
                echo ($vec2[$i]);
            }
            echo("<br>");*/
        }
        else
        {
            for($i=0; $i<$j; $i++)
            {
                $vec2[$i]=$vec[$i];
            }
            
            $vec2[$j]=$total;
            
            $cont=$j+1;
            
            for($i=($fin+1); $i<sizeof($vec); $i++)
            {
                $vec2[$cont]=$vec[$i];
                $cont++;
            }


            /*echo("<br>");
            for($i=0; $i<sizeof($vec2); $i++)
            {
                echo($vec2[$i]);
            }
            echo("<br>");*/
        
            return $vec2;
        }
    } 

?>