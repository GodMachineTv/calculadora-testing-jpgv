
$(document).ready(function(){

    $('body').on('click', '#euler', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("e"); 

            }
            else
            {
                if(!isNaN(digde) || digde=='e' || digde=='π')
                {
                    recur=0;
                    for(i=0; i<digiz.length; i++)
                    {
                        if(digiz[i]=="%")
                        {
                            recur++;
                            break;
                        }
                    }


                    anterior=anterior.trim();


                    if(recur>0)
                    {
                        if(anterior=="de" || anterior== "porcentaje equivalente de")
                        {
                            if(digde=='e' || digde=='π')
                            {
                                document.getElementById("digde").textContent = "e"; 
                            }
                            else
                            {
                                alert("error, porcentaje");
                            }
                        }
                    }
                    else
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append("x");
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("x");
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "e";
                    }
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "e";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')' || anterior=='π' || anterior=='e')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "e";
                            }
                            else
                            {
                                signo='';

                                if(digde=='+')
                                {
                                    signo='+';
                                }
                                else
                                {
                                    signo='-';
                                }

                                $("#digiz").append(signo+"1");
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(signo+"1");
                                value++;
                                document.getElementById("general").className = value;
                                $("#digiz").append("x");
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append("x");
                                value++;
                                document.getElementById("general").className = value;

                                document.getElementById("digde").textContent = "e";
                            }
                        }
                        else
                        {
                            if(digde==" de " || digde== " porcentaje equivalente de ")
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "e";    
                            }
                            else
                            {
                                alert("no es posible realizar esta operacion, error cero value");
                            }
                        }
                    }
                }
            }
            
            /*for(i=0; i<value; i++)
            {
                alert(document.getElementById("digiz" + i).textContent);
            }*/
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("e");
            }
            else
            {
                if(digde=='+' || digde=='-')
                {
                    $("#digde").append("e");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "e";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e' || !isNaN(digde))
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "e";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error cero");
                        }
                        
                    }
                }
            }
        }   
    });

    
    $('body').on('click', '#pi', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("π"); 

            }
            else
            {
                if(!isNaN(digde) || digde=='e' || digde=='π')
                {
                    recur=0;
                    for(i=0; i<digiz.length; i++)
                    {
                        if(digiz[i]=="%")
                        {
                            recur++;
                            break;
                        }
                    }


                    anterior=anterior.trim();


                    if(recur>0)
                    {
                        if(anterior=="de" || anterior== "porcentaje equivalente de")
                        {
                            if(digde=='e' || digde=='π')
                            {
                                document.getElementById("digde").textContent = "π"; 
                            }
                            else
                            {
                                alert("error, porcentaje");
                            }
                        }
                    }
                    else
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append("x");
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("x");
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "π";
                    }

                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "π";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "π";
                            }
                            else
                            {
                                signo='';

                                if(digde=='+')
                                {
                                    signo='+';
                                }
                                else
                                {
                                    signo='-';
                                }

                                $("#digiz").append(signo+"1");
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(signo+"1");
                                value++;
                                document.getElementById("general").className = value;
                                $("#digiz").append("x");
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append("x");
                                value++;
                                document.getElementById("general").className = value;

                                document.getElementById("digde").textContent = "π";
                            }
                            
                        }
                        else
                        {
                            if(digde==" de " || digde== " porcentaje equivalente de ")
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "π";    
                            }
                            else
                            {
                                alert("no es posible realizar esta operacion, error cero value");
                            }
                        }
                    }
                }
            }
            
            /*for(i=0; i<value; i++)
            {
                alert(document.getElementById("digiz" + i).textContent);
            }*/
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("π");
            }
            else
            {
                if(digde=='+' || digde=='-')
                {
                    $("#digde").append("e");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "π";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e' || !isNaN(digde))
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "π";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error cero");
                        }
                        
                    }
                }
            }
        }   
    });


    $('body').on('click', '#cero', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("0"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("0"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "0";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "0";
                            }
                            else
                            {
                                $("#digde").append("0");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "0";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "0";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
            
            /*for(i=0; i<value; i++)
            {
                alert(document.getElementById("digiz" + i).textContent);
            }*/
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {
                $("#digde").append("0");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("0");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "0";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "0";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error cero");
                        }   
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#uno', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("1"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("1"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "1";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "1";
                            }
                            else
                            {
                                $("#digde").append("1");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "1";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "1";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("1");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("1");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "1";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "1";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error uno value");
                        }
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#dos', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("2"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("2"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "2";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "2";
                            }
                            else
                            {
                                $("#digde").append("2");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "2";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "2";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("2");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("2");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "2";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "2";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error dos value");
                        }
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#tres', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("3"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("3"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "3";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "3";
                            }
                            else
                            {
                                $("#digde").append("3");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "3";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "3";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("3");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("3");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "3";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "3";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error tres value");
                        }
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#cuatro', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("4"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("4"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "4";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "4";
                            }
                            else
                            {
                                $("#digde").append("4");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "4";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "4";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("4");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("4");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "4";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "4";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error cuatro value");
                        }
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#cinco', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("5"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("5"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "5";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "5";
                            }
                            else
                            {
                                $("#digde").append("5");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "5";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "5";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("5");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("5");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "5";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "5";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error cinco value");
                        }
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#seis', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("6"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("6"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "6";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "6";
                            }
                            else
                            {
                                $("#digde").append("6");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "6";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "6";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("6");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("6");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "6";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "6";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error seis value");
                        }
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#siete', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("7"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("7"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "7";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "7";
                            }
                            else
                            {
                                $("#digde").append("7");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "7";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "7";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("7");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("7");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "7";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "7";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error siete value");
                        }
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#ocho', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("8"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("8"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "8";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "8";
                            }
                            else
                            {
                                $("#digde").append("8");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "8";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "8";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("8");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("8");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "8";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "8";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error ocho value");
                        }
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#nueve', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("9"); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digde").append("9"); 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/'  || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "9";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior) || anterior==')')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "9";
                            }
                            else
                            {
                                $("#digde").append("9");
                            }
                            
                        }
                        else
                        {
                            if(digde=='π' || digde=='e')
                            {
                                recur=0;
                                for(i=0; i<digiz.length; i++)
                                {
                                    if(digiz[i]=="%")
                                    {
                                        recur++;
                                        break;
                                    }
                                }


                                anterior=anterior.trim();


                                if(recur>0)
                                {
                                    if(anterior=="de" || anterior== "porcentaje equivalente de")
                                    {
                                        alert("error, porcentaje");
                                    }
                                }
                                else
                                {     
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;
                                    $("#digiz").append("x");
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append("x");
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "9";
                                }
                            }
                            else
                            {
                                if(digde==" de " || digde==" porcentaje equivalente de ")
                                {
                                    $("#digiz").append(digde);
                                    $("#aux").append("<span id='digiz" + value +"'></span>");
                                    $("#digiz"+value).append(digde);
                                    value++;
                                    document.getElementById("general").className = value;

                                    document.getElementById("digde").textContent = "9";
                                }
                                else
                                {
                                    alert("no es posible realizar esta operacion, error cero value");
                                }       
                            }
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("9");
            }
            else
            {
                if(digde=='+' || digde=='-' || !isNaN(digde))
                {
                    $("#digde").append("9");
                }
                else
                {
                    if(digde=='x' || digde=='^' || digde=='√' || digde=='/' || digde=='(')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "9";
                    }
                    else
                    {
                        if(digde=='π' || digde=='e')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append("x");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("x");
                            value++;
                            document.getElementById("general").className = value;

                            document.getElementById("digde").textContent = "9";
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error nueve value");
                        }
                    }
                }
            }
        }   
    });
    
    
    
    $('body').on('click', '#mas', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || anterior==')' || anterior=='π' || anterior=='e')
                {
                    $("#digde").append("+");    
                }
                else
                {     
                    if(anterior=='(')
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("0");
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append("0");
                        document.getElementById("digde").textContent = "+";
                    }
                    else
                    {
                        if(anterior=="x" || anterior=='/' || anterior=='^' || anterior=='√')
                        {
                            $("#digde").append("+");
                        }
                        else
                        {
                            if(anterior=='+' || anterior=='-')
                            {
                                document.getElementById("digde").textContent = "+";
                            }
                        }
                    }    
                }
            }
            else
            {
                if(!isNaN(digde) || digde=='π' || digde=='e')
                {
                    $("#digiz").append(digde);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    document.getElementById("digde").textContent = "+";
                }
                else
                {
                    if(digde=='(')
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("0");
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        $("#digiz").append("0");
                        document.getElementById("digde").textContent = "+";
                    }
                    else
                    {
                        if(digde=="x" || digde=='/' || digde=='^' || digde=='√' || digde==')')
                        {
                             $("#digiz").append(digde);
                             $("#aux").append("<span id='digiz" + value +"'></span>");
                             $("#digiz"+value).append(digde);
                             value++;
                             document.getElementById("general").className = value;
                             document.getElementById("digde").textContent = "+";
                        }
                        else
                        {
                            if(digde=='+' || digde=='-')
                            {
                                document.getElementById("digde").textContent = "+";
                            }
                        }
                    }
                }
            }
            
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {
                $("#aux").append("<span id='digiz" + value +"'></span>");
                $("#digiz"+value).append("0");
                value++;
                document.getElementById("general").className = value;
                $("#digiz").append("0");
                $("#digde").append("+");
                
            }
            else
            {
                if(digde=='(')
                {
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("0");
                    value++;
                    document.getElementById("general").className = value;
                    $("#digiz").append(digde);
                    $("#digiz").append("0");
                    document.getElementById("digde").textContent = "+";
                }
                else
                {
                    if(digde=='+' || digde=='-' || digde=='^' || digde=='√' || digde=='x' || digde=='/')
                    {
                        document.getElementById("digde").textContent = "+";
                    }
                    else
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        document.getElementById("digde").textContent = "+";
                    }
                }  
            }
        }  
    });
    
    
    
    $('body').on('click', '#menos', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || anterior==')' || anterior=='π' || anterior=='e')
                {
                    $("#digde").append("-");    
                }
                else
                {     
                    if(anterior=='(')
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("0");
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append("0");
                        document.getElementById("digde").textContent = "-";
                    }
                    else
                    {
                        if(anterior=="x" || anterior=='/' || anterior=='^' || anterior=='√')
                        {
                            $("#digde").append("-");
                        }
                        else
                        {
                            if(anterior=='+' || anterior=='-')
                            {
                                document.getElementById("digde").textContent = "-";
                            }
                        }
                    }    
                }
            }
            else
            {
                if(!isNaN(digde) || digde=='π' || digde=='e')
                {
                    $("#digiz").append(digde);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    document.getElementById("digde").textContent = "-";
                }
                else
                {
                    if(digde=='(')
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("0");
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        $("#digiz").append("0");
                        document.getElementById("digde").textContent = "-";
                    }
                    else
                    {
                        if(digde=="x" || digde=='/' || digde=='^' || digde=='√' || digde==')')
                        {
                             $("#digiz").append(digde);
                             $("#aux").append("<span id='digiz" + value +"'></span>");
                             $("#digiz"+value).append(digde);
                             value++;
                             document.getElementById("general").className = value;
                             document.getElementById("digde").textContent = "-";
                        }
                        else
                        {
                            if(digde=='+' || digde=='-')
                            {
                                document.getElementById("digde").textContent = "-";
                            }
                        }
                    }
                }
            }
            
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {
                $("#aux").append("<span id='digiz" + value +"'></span>");
                $("#digiz"+value).append("0");
                value++;
                document.getElementById("general").className = value;
                $("#digiz").append("0");
                $("#digde").append("-");
                
            }
            else
            {
                if(digde=='(')
                {
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("0");
                    value++;
                    document.getElementById("general").className = value;
                    $("#digiz").append(digde);
                    $("#digiz").append("0");
                    document.getElementById("digde").textContent = "-";
                }
                else
                {
                    if(digde=='+' || digde=='-' || digde=='^' || digde=='√' || digde=='x' || digde=='/')
                    {
                        document.getElementById("digde").textContent = "-";
                    }
                    else
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        document.getElementById("digde").textContent = "-";
                    }
                }  
            }
        }  
    });
    
    
    $('body').on('click', '#coma', function()
    {
        var digde = document.getElementById("digde").textContent;
        
        var value = $('#general').attr('class');
        
        var i=0;
        
        if(value>0)
        {    
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                
                $("#digde").append("0."); 

            }
            else
            {
                if(!isNaN(digde))
                {
                    if (digde%1==0) 
                    {
                        var bandera=0;
                            
                        for(i=0; i<digde.length; i++)
                        {
                            if(digde[i]=='.')
                            {
                                bandera=1;
                            }
                        }
                            
                        if(bandera==1)
                        {
                            alert("no es posible realizar esta accion, el numero ya es decimal");
                        }
                        else
                        {
                            $("#digde").append(".");
                        }  
                    } 
                    else 
                    {
                        alert("no es posible realizar esta accion, el numero ya es decimal");
                    }; 
                }
                else
                {
                    if(digde=='(' || digde=="x" || digde=='/'  || digde=='^' || digde=='√')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "0.";
                    }
                    else
                    {
                        if(digde=='+' || digde=='-')
                        {
                            if(!isNaN(anterior))
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append(digde);
                                value++;
                                document.getElementById("general").className = value;
                                document.getElementById("digde").textContent = "0.";
                            }
                            else
                            {
                                $("#digde").append("0.");
                            }
                            
                        }
                        else
                        {
                            alert("no es posible realizar esta operacion, error coma value");
                        }
                        
                    }
                }
            }
        }
        else
        {
            if(digde=="")
            {
                $("#digde").append("0.");
            }
            else
            {
                if(digde=='+' || digde=='-' || digde=='(' || digde==')' || digde=='^' || digde=='√')
                {
                    $("#digde").append("0.");
                }
                else
                {
                    if(!isNaN(digde))
                    {
                        if (digde%1==0) 
                        {
                            var bandera=0;
                            
                            for(i=0; i<digde.length; i++)
                            {
                                if(digde[i]=='.')
                                {
                                    bandera=1;
                                }
                            }
                            
                            if(bandera==1)
                            {
                               alert("no es posible realizar esta accion, el numero ya es decimal");
                            }
                            else
                            {
                                $("#digde").append(".");
                            }  
                        } 
                        else 
                        {
                            alert("no es posible realizar esta accion, el numero ya es decimal");
                        }
                    }
                    else
                    {
                        alert("no es posible realizar esta operacion, error coma");
                    }
                }
            }
        }   
    });
    
    
    $('body').on('click', '#por', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || anterior=='π' || anterior=='e')
                {
                    $("#digde").append("x");    
                }
                else
                {
                    if(anterior==')')
                    {
                        $("#digde").append("x"); 
                    }
                    else
                    {
                        if(anterior=='x' || anterior=='/' || anterior=='+' || anterior=='-' || anterior=='^' || anterior=='√')
                        {
                            document.getElementById("digiz").textContent=digiz.substring(0,digiz.length-1);
                            $("#digiz").append("x");
                            document.getElementById("digiz" + (value-1)).textContent="x";
                        
                            //alert(document.getElementById("digiz").textContent);
                        }
                        else
                        {
                            if(anterior=='(')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append("0");
                                value++;
                                document.getElementById("general").className = value;
                                
                                $("#digde").append("x"); 
                            }
                        }    
                    }
                }
            }
            else
            {
                if(!isNaN(digde) || digde=='π' || digde=='e')
                {
                    $("#digiz").append(digde);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    document.getElementById("digde").textContent = "x";
                }
                else
                {
                    if(digde==')')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        
                        document.getElementById("digde").textContent = "x";
                                
                    }
                    else
                    {
                        if(digde=='+'|| digde=='-' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                        {
                            //document.getElementById("digiz").textContent=digiz.substring(0,digiz.length-1);
                            //$("#digiz").append("x");
                            //document.getElementById("digiz" + (value-1)).textContent="x";
                            if(anterior=='x' || anterior=='/' || anterior=='^' || anterior=='√')
                            {
                                alert("No es posible realizar esta operacion, error doble por");
                            }
                            else
                            {
                                document.getElementById("digde").textContent = "x";
                            }
                            
                        }
                        else
                        {
                            if(digde=='(')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append("0");
                                value++;
                                document.getElementById("general").className = value;
                                
                                $("#digde").append("x"); 
                            }
                        }
                    }
                }
            }
            
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {
                $("#aux").append("<span id='digiz" + value +"'></span>");
                $("#digiz"+value).append("0");
                value++;
                document.getElementById("general").className = value;
                $("#digiz").append("0");
                $("#digde").append("x");
                //document.getElementById("digde").textContent = "x";  
            }
            else
            {
                if(digde=='+' || digde=='-' || digde=='x' || digde=='/' || digde=='^' || digde=='√')
                {
                    document.getElementById("digde").textContent = "x";
                }
                else
                {
                    if(digde=='(')
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("0");
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        $("#digiz").append("0");
                        document.getElementById("digde").textContent = "x";    
                    }
                    else
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        document.getElementById("digde").textContent = "x";    
                    }
                    
                }   
            }
        }  
    });
    
    
    $('body').on('click', '#division', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || anterior=='π' || anterior=='e')
                {
                    $("#digde").append("/");    
                }
                else
                {
                    if(anterior==')')
                    {
                        $("#digde").append("/"); 
                    }
                    else
                    {
                        if(anterior=='x' || anterior=='/' || anterior=='+' || anterior=='-' || anterior=='^' || anterior=='√')
                        {
                            document.getElementById("digiz").textContent=digiz.substring(0,digiz.length-1);
                            $("#digiz").append("/");
                            document.getElementById("digiz" + (value-1)).textContent="x";
                        
                            //alert(document.getElementById("digiz").textContent);
                        }
                        else
                        {
                            if(anterior=='(')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append("0");
                                value++;
                                document.getElementById("general").className = value;
                                
                                $("#digde").append("/"); 
                            }
                        }    
                    }
                }
            }
            else
            {
                if(!isNaN(digde) || digde=='π' || digde=='e')
                {
                    $("#digiz").append(digde);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    document.getElementById("digde").textContent = "/";
                }
                else
                {
                    if(digde==')')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        
                        document.getElementById("digde").textContent = "/";
                                
                    }
                    else
                    {
                        if(digde=='+'|| digde=='-' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                        {
                            //document.getElementById("digiz").textContent=digiz.substring(0,digiz.length-1);
                            //$("#digiz").append("x");
                            //document.getElementById("digiz" + (value-1)).textContent="x";
                            if(anterior=='x' || anterior=='/' || anterior=='^' || anterior=='√')
                            {
                                alert("No es posible realizar esta operacion, error doble division");
                            }
                            else
                            {
                                document.getElementById("digde").textContent = "/";
                            }
                            
                        }
                        else
                        {
                            if(digde=='(')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append("0");
                                value++;
                                document.getElementById("general").className = value;
                                
                                $("#digde").append("/"); 
                            }
                        }
                    }
                }
            }
            
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {            
                $("#aux").append("<span id='digiz" + value +"'></span>");
                $("#digiz"+value).append("0");
                value++;
                document.getElementById("general").className = value;
                $("#digiz").append("0");
                $("#digde").append("/");
                //document.getElementById("digde").textContent = "x";  
            }
            else
            {
                if(digde=='+' || digde=='-' || digde=='x' || digde=='/' || digde=='^' || digde=='√')
                {
                    document.getElementById("digde").textContent = "/";
                }
                else
                {
                    if(digde=='(')
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("0");
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        $("#digiz").append("0");
                        document.getElementById("digde").textContent = "/";    
                    }
                    else
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        document.getElementById("digde").textContent = "/";    
                    }
                }   
            }
        }  
    });
    
    
    $('body').on('click', '#pariz', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || anterior=='π' || anterior=='e')
                {
                    alert("ingrese una operacion con antelacion, error parentesis 2");
                }
                else
                {
                    if(anterior==')')
                    {
                        alert("ingrese una operacion con antelacion, error parentesis 3");
                    }
                    else
                    {
                        if(anterior=='x' || anterior=='/' || anterior=='+' || anterior=='-' || anterior=='^' || anterior=='√' || anterior=='(')
                        {
                            $("#digiz").append("(");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("(");
                            value++;
                            document.getElementById("general").className = value;
                                
                            $("#digde").append("("); 
                            document.getElementById("pariz").name++;
                            //alert(document.getElementById("digiz").textContent);
                        }
                        else
                        {
                            alert("numero pi y euler aun no los hago");
                        }    
                    }
                }
            }
            else
            {
                if(!isNaN(digde) || digde=='π' || digde=='e')
                {
                    alert("ingrese una operacion con antelacion, error parentesis 4");
                }
                else
                {
                    if(digde==')')
                    {
                        alert("ingrese una operacion con antelacion, error parentesis 5");
                    }
                    else
                    {
                        if(digde=='+'|| digde=='-' || digde=="x" || digde=='/' || digde=='^' || digde=='√' || digde=='(')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            document.getElementById("pariz").name++;
                            
                            document.getElementById("digde").textContent="(";
                        }
                        else
                        {
                            alert("no es posible realizar esta accion");
                           
                        }
                    }
                }
            }    
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {            
                $("#digde").append("(");
                document.getElementById("pariz").name++;
                //alert(document.getElementById("pariz").name);
                //document.getElementById("digde").textContent = "x";  
            }
            else
            {
                if(digde=='+' || digde=='-' || digde=='x' || digde=='/' || digde=='^' || digde=='√' || digde=='(')
                {
                    $("#digiz").append(digde);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    document.getElementById("digde").textContent = "(";
                    document.getElementById("pariz").name++;
                }
                else
                {
                    alert("Ingrese una operacion con anterioridad, error parentesis");
                    
                }   
            }
        }  
    });
    
    
    
    $('body').on('click', '#parde', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || anterior==')' || anterior=='π' || anterior=='e')
                {
                    if(document.getElementById("pariz").name>0)
                    {
                        $("#digde").append(")"); 
                        document.getElementById("pariz").name--;
                    }
                    else
                    {
                        alert("No se encontro parentesis de apertura (, error parentesis 2.3");
                    }    
                }
                else
                {
                    alert("No es posible cerrar parentesis sin un numero previo, error parentess 2.4");
                }
            }
            else
            {
                if(!isNaN(digde) || digde==')' || digde=='π' || digde=='e')
                {
                    if(document.getElementById("pariz").name>0)
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("pariz").name--;
                            
                        document.getElementById("digde").textContent=")";
                    }
                    else
                    {
                        alert("No se encontro parentesis de apertura (, error parentesis 2.5");
                    }
                }
                else
                {
                    alert("No es posible cerrar parentesis sin un numero previo, error parentess 2.6");
                }
            }    
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {            
                alert("Ingrese parentesis de apertura ( y minumo un numero, error parentesis 2.0"); 
            }
            else
            {
                if(digde=='+' || digde=='-' || digde=='x' || digde=='/' || digde=='^' || digde=='√' || digde=='(')
                {
                    alert("Ingrese parentesis de apertura ( y minumo un numero, error parentesis 2.1"); 
                }
                else
                {
                    if(document.getElementById("pariz").name>0)
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = ")";
                        document.getElementById("pariz").name--;
                    }
                    else
                    {
                        alert("No se encontro parentesis de apertura ( inicial, errp áremtesos 2.2");
                    }
                    
                }   
            }
        }  
    });
    
    
    $('body').on('click', '#potencia', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || digde=='π' || digde=='e')
                {
                    $("#digde").append("^");    
                }
                else
                {
                    if(anterior==')')
                    {
                        $("#digde").append("^"); 
                    }
                    else
                    {
                        if(anterior=='x' || anterior=='/' || anterior=='+' || anterior=='-' || anterior=='^' || anterior=='√')
                        {
                            document.getElementById("digiz").textContent=digiz.substring(0,digiz.length-1);
                            $("#digiz").append("^");
                            document.getElementById("digiz" + (value-1)).textContent="^";
                        
                            //alert(document.getElementById("digiz").textContent);
                        }
                        else
                        {
                            if(anterior=='(')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append("0");
                                value++;
                                document.getElementById("general").className = value;
                                
                                $("#digde").append("^"); 
                            }
                        }    
                    }
                }
            }
            else
            {
                if(!isNaN(digde) || digde=='π' || digde=='e')
                {
                    $("#digiz").append(digde);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    document.getElementById("digde").textContent = "^";
                }
                else
                {
                    if(digde==')')
                    {
                        $("#digiz").append(digde);
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        
                        document.getElementById("digde").textContent = "^";
                                
                    }
                    else
                    {
                        if(digde=='+'|| digde=='-' || digde=="x" || digde=='/' || digde=='^' || digde=='√')
                        {
                            //document.getElementById("digiz").textContent=digiz.substring(0,digiz.length-1);
                            //$("#digiz").append("x");
                            //document.getElementById("digiz" + (value-1)).textContent="x";
                            if(anterior=='x' || anterior=='/' || anterior=='^' || anterior=='√')
                            {
                                alert("No es posible realizar esta operacion, error doble por");
                            }
                            else
                            {
                                document.getElementById("digde").textContent = "^";
                            }
                            
                        }
                        else
                        {
                            if(digde=='(')
                            {
                                $("#digiz").append(digde);
                                $("#aux").append("<span id='digiz" + value +"'></span>");
                                $("#digiz"+value).append("0");
                                value++;
                                document.getElementById("general").className = value;
                                
                                $("#digde").append("^"); 
                            }
                        }
                    }
                }
            }
            
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {
                $("#aux").append("<span id='digiz" + value +"'></span>");
                $("#digiz"+value).append("0");
                value++;
                document.getElementById("general").className = value;
                $("#digiz").append("0");
                $("#digde").append("^");
                //document.getElementById("digde").textContent = "x";  
            }
            else
            {
                if(digde=='+' || digde=='-' || digde=='x' || digde=='/' || digde=='^' || digde=='√')
                {
                    document.getElementById("digde").textContent = "^";
                }
                else
                {
                    if(digde=='(')
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("0");
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        $("#digiz").append("0");
                        document.getElementById("digde").textContent = "^";    
                    }
                    else
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        document.getElementById("general").className = value;
                        $("#digiz").append(digde);
                        document.getElementById("digde").textContent = "^";    
                    }
                    
                }   
            }
        }  
    });
    
    
    
    $('body').on('click', '#raiz', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || digde=='π' || digde=='e')
                {
                    alert("Ingrese una operacion previamente, error raiz 2");    
                }
                else
                {
                    if(anterior==')')
                    {
                        alert("Ingrese una operacion previamente, error raiz 3");    
                    }
                    else
                    {
                        if(anterior=='x' || anterior=='/' || anterior=='+' || anterior=='-' || anterior=='^' || anterior=='√' || anterior=='(')
                        {
                            $("#digde").append("√"); 
                            //alert(document.getElementById("digiz").textContent);
                        }
                    }
                }
            }
            else
            {
                if(!isNaN(digde) || digde=='π' || digde=='e')
                {
                     alert("Ingrese una operacion previamente, error raiz 4"); 
                }
                else
                {
                    if(digde==')')
                    {
                         alert("Ingrese una operacion previamente, error raiz 5"); 
                    }
                    else
                    {
                        if(digde=='+'|| digde=='-' || digde=="x" || digde=='/' || digde=='^' || digde=='√' || digde=='(')
                        {
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            document.getElementById("general").className = value;
                            $("#digiz").append(digde);
                            document.getElementById("digde").textContent = "√"; 
                        }
                    }
                }
            }
            
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {
                $("#digde").append("√");
                //document.getElementById("digde").textContent = "x";  
            }
            else
            {
                if(digde=='+' || digde=='-' || digde=='x' || digde=='/' || digde=='^' || digde=='√' || digde=='(')
                {
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    $("#digiz").append(digde);
                    document.getElementById("digde").textContent = "√";  
                }
                else
                {
                    alert("ingrese una operacion previamente, error raiz");                    
                }   
            }
        }  
    });
    

    $('body').on('click', '#log', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        if(porc==1)
        {
            return alert("No es posible aplicar esta funcion utilizando %.");
        }
        
        if(value>0)
        {
            var anterior = document.getElementById("digiz" + (value-1)).textContent;
            
            if(digde=="")
            {
                if(!isNaN(anterior) || anterior=='π' || anterior=='e')
                {
                    alert("ingrese una operacion con antelacion, error log 2");
                }
                else
                {
                    if(anterior==')')
                    {
                        alert("ingrese una operacion con antelacion, error log 3");
                    }
                    else
                    {
                        if(anterior=='x' || anterior=='/' || anterior=='+' || anterior=='-' || anterior=='^' || anterior=='√' || anterior=='(')
                        {
                            $("#digiz").append("log");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("log");
                            value++;
                            document.getElementById("general").className = value;
                                
                            $("#digde").append("("); 
                            document.getElementById("pariz").name++;
                            //alert(document.getElementById("digiz").textContent);
                        }
                        else
                        {
                            alert("numero pi y euler aun no los hago");
                        }    
                    }
                }
            }
            else
            {
                if(!isNaN(digde) || digde=='π' || digde=='e')
                {
                    alert("ingrese una operacion con antelacion, error log 4");
                }
                else
                {
                    if(digde==')')
                    {
                        alert("ingrese una operacion con antelacion, error log 5");
                    }
                    else
                    {
                        if(digde=='+'|| digde=='-' || digde=="x" || digde=='/' || digde=='^' || digde=='√' || digde=='(')
                        {
                            $("#digiz").append(digde);
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append(digde);
                            value++;
                            $("#digiz").append("log");
                            $("#aux").append("<span id='digiz" + value +"'></span>");
                            $("#digiz"+value).append("log");
                            value++;
                            document.getElementById("general").className = value;
                            document.getElementById("pariz").name++;
                            
                            document.getElementById("digde").textContent="(";
                        }
                        else
                        {
                            alert("no es posible realizar esta accion");
                           
                        }
                    }
                }
            }    
        }
        else
        {
            //alert(value);
            //alert(digde);
            if(digde=="")
            {            
                $("#digde").append("(");
                document.getElementById("pariz").name++;
                $("#aux").append("<span id='digiz" + value +"'></span>");
                $("#digiz"+value).append("log");
                value++;
                document.getElementById("general").className = value;
                $("#digiz").append("log");

            }
            else
            {
                if(digde=='+' || digde=='-' || digde=='x' || digde=='/' || digde=='^' || digde=='√' || digde=='(')
                {
                    $("#digiz").append(digde);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    $("#digiz").append("log");
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("log");
                    value++;
                    document.getElementById("general").className = value;
                    document.getElementById("digde").textContent = "(";
                    document.getElementById("pariz").name++;
                }
                else
                {
                    alert("Ingrese una operacion con anterioridad, error log");
                    
                }   
            }
        }  
    });

    
    $('body').on('click', '#fraccion', function()
    {
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent; 
        var value = $('#general').attr('class');

        var porc=0, v;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=='%')
            {
                porc=1;
                break;
            }
            else
            {
                porc=0;
            }
        }

        
        if(digde=="" || porc==1)
        {
            if(digde=="")
            {
                alert("ingrese un numero, preferentemente decimal.");
            }
            else
            {
                alert("no es posible realizar esta operacion utilizando %.");
            }
        }
        else
        {
            if(!isNaN(digde))
            {
                var largo=digde.length;
                var aux=parseInt(digde);

                if(aux==digde)
                {
                    document.getElementById("digde").textContent=")";

                    $("#digiz").append("(");
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("(");
                    value++;
                    $("#digiz").append(aux);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(aux);
                    value++;
                    $("#digiz").append("/");
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("/");
                    value++;
                    $("#digiz").append("1");
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("1");
                    value++;
                    document.getElementById("general").className = value;
                }
                else
                {
                    var coma="1", i, numero;

                    for(i=0; i<digde.length; i++)
                    {
                        if(digde[i]=='.')
                        {
                            aux=i;
                        }    
                    }

                    for(i=aux+1; i<digde.length; i++)
                    {
                        coma=coma+"0";
                    }

                    numero=digde*coma;

                    var bandera=0, mayor, menor, mcm=0;

                    if(numero>coma)
                    {
                        mayor=numero;
                        menor=coma;
                    }
                    else
                    {
                        mayor=coma;
                        menor=numero;
                    }



                    while(bandera==0)
                    {
                        aux=0;
                        mcm=0;

                        if(menor==numero)
                        {
                            for(i=2; i<=menor; i++)
                            {
                                if(numero%i==0 && coma%i==0)
                                {
                                    numero=numero/i;
                                    coma=coma/i;
                                    mcm=1;
                                }
                            }

                            if(i==menor)
                            {
                                bandera=1;
                            }
                        }
                        else
                        {
                            for(i=2; i<menor; i++)
                            {
                                if(numero%i==0 && coma%i==0)
                                {
                                    numero=numero/i;
                                    coma=coma/i;
                                    mcm=1;
                                }
                            }

                            if(i==menor-1)
                            {
                                bandera=1;
                            }
                        }

                        
                        if(numero>coma)
                        {
                            mayor=numero;
                            menor=coma;
                        }
                        else
                        {
                            mayor=coma;
                            menor=numero;
                        }

                        for(i=1; i<menor; i++)
                        {
                            if(menor%i==0)
                            {
                                aux++;

                                if(aux>1)
                                {
                                    break;
                                }
                            }
                        }

                        if(aux==1)
                        {
                            bandera=1;
                        }

                        if(menor==numero && menor==1)
                        {
                            bandera=1;
                        }

                        if(mcm==0)
                        {
                            bandera=1;
                        }
                    }

                    document.getElementById("digde").textContent=")";

                    $("#digiz").append("(");
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("(");
                    value++;
                    $("#digiz").append(numero);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(numero);
                    value++;
                    $("#digiz").append("/");
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("/");
                    value++;
                    $("#digiz").append(coma);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(coma);
                    value++;
                    document.getElementById("general").className = value;
                }             

            }
            else
            {
                alert("solo es posible realizar la conversion a numeros");
            }
        }
        
    });


    $('body').on('click', '#porcentaje', function()
    {
        var value = $('#general').attr('class');
        var digde = document.getElementById("digde").textContent;

        if(value==0)
        {
            if(digde=="")
            {
                alert("ingrese un numero previamente");
            }
            else
            {
                if(!isNaN(digde))
                {
                    $("#digiz").append(digde);
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    $("#digiz").append("%");
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append("%");
                    value++;
                    document.getElementById("digde").textContent=" de ";
                    document.getElementById("general").className = value;
                }
                else
                {
                    alert("no es posible realizar esta operacion sin numeros, limpie la pantalla e ingrese un numero");
                }
            }
        }
        else
        {
            if(digde==" de ")
            {
                document.getElementById("digde").textContent=" porcentaje equivalente de ";
            }
            else
            {
                if(digde==" porcentaje equivalente de ")
                {
                    document.getElementById("digde").textContent=" de ";
                }
                else
                {
                    alert("no es posible utilizar esta operacion con multiples operandos, limpie la pantalla, ingrese un numero y presione el boton %");
                }
            }            
        }
    });


    $('body').on('click', '#igual', function()
    {
        var maximo=document.getElementById("pariz").name;
        var digde = document.getElementById("digde").textContent;
        var digiz = document.getElementById("digiz").textContent;
        var value = $('#general').attr('class');
        
        var porc=0, v, valiporc=0;

        for(v=0; v<digiz.length; v++)
        {
            if(digiz[v]=="%")
            {
                porc=1;
            }
            else
            {
                if(porc==1)
                {
                    valiporc++;
                }
                else
                {
                    porc=0;
                }
            }
        }

        if(porc==1)
        {
            if(valiporc>0)
            {
                $("#digiz").append(digde);

                $("#aux").append("<span id='digiz" + value +"'></span>");
                $("#digiz"+value).append(digde);
                value++;
                document.getElementById("general").className = value;
                document.getElementById("digde").textContent = "";

                var vec=[];

                for(v=0; v<(value); v++)
                {
                    vec[v]= document.getElementById("digiz"+v).textContent;
                }


                $.ajax(
                {
                    url: 'controlador/2 porcentaje.php',
                    type: 'POST',
                    data: {'arreglo':vec}, 
                    dataType: 'json',
                })
        
                .done(function(data) 
                {
                    alert(data);         
                })

                .fail(function() 
                {
                    alert("Error al verificar!");
                });  

                alert("ss");
                return 0;
            }
            else
            {
                return alert("Ingrese el numero final para calcular el %.");
            }
        }
        
        var bandera=0;
        
        if(value>0)
        {
            if(!isNaN(digde) || digde=='π' || digde=='e')
            {
                var anterior = document.getElementById("digiz" + (value-1)).textContent;
                
                if(!isNaN(anterior))
                {
                    $("#digiz"+(value-1)).append(digde);
                    ("#digiz").append(digde);
                    document.getElementById("digde").textContent = "";  
                }
                else
                {
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    $("#digiz").append(digde);
                    document.getElementById("digde").textContent = "";  
                }
            }
            else
            {
                if(digde==')')
                {
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(digde);
                    value++;
                    document.getElementById("general").className = value;
                    $("#digiz").append(digde);
                    document.getElementById("digde").textContent = "";  
                }
                else
                {
                    if(digde=='+'|| digde=='-' || digde=="x" || digde=='/' || digde=='^' || digde=='√' || digde=='(')
                    {
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append(digde);
                        value++;
                        $("#aux").append("<span id='digiz" + value +"'></span>");
                        $("#digiz"+value).append("0");
                        value++;
                        $("#digiz").append(digde);
                        $("#digiz").append("0");
                        document.getElementById("general").className = value;
                        document.getElementById("digde").textContent = "";  
                    }
                    else
                    {
                        alert("no hay cambios");
                    }
                }
            }
            
            if(maximo>0)
            {
                do
                {
                    $("#aux").append("<span id='digiz" + value +"'></span>");
                    $("#digiz"+value).append(")");
                    value++;
                    $("#digiz").append(")");
                    document.getElementById("general").className = value;
                    document.getElementById("digde").textContent = "";  
                    maximo--;
                }while(maximo>0);  
            }
            
            
        }
        else
        {
            alert("ingrese mas factores");
            bandera=1;
        }
        
        var vec=[];
        
        for(i=0; i<(value); i++)
        {
            vec[i]= document.getElementById("digiz"+i).textContent;
        }
        

        if(bandera!=1)
        {
            for(i=0; i<vec.length; i++)
            {
                if(vec[i]=='e')
                {
                    vec[i]=2.718281828459;
                }
                else
                {
                    if(vec[i]=='π')
                    {
                        vec[i]=3.1415926535897;
                    }
                }
            }

            //alert(vec);
            
            
            $.ajax(
            {
                url: 'controlador/1 calcular.php',
                type: 'POST',
                data: {'arreglo':vec}, 
                dataType: 'json',
            })
        
            .done(function(data) 
            {
                for(i=0; i<data.length; i++)
                {
                    alert(data[i]);
                }         
            })

            .fail(function() 
            {
      	        alert("Error al verificar!");
            });        
        }
    });

});


