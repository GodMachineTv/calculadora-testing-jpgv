<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Calculadora</title>


		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

		<!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css"/>

		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="assets/css/slick.css"/>
		<link type="text/css" rel="stylesheet" href="assets/css/slick-theme.css"/>

		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="assets/css/nouislider.min.css"/>

		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">

		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="assets/css/estilo8.css"/>



    </head>
	<body onload="inicio()">

		<div id="cargando">
			<span id="calculando_icono"><!--<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>--></span>
            <!--<span class="sr-only">Loading...</span>-->
            <span id="calculando"></span>
		</div>

		<div id="medidas" class='0'>
			<!--<div id="medidas_menu">
				<h1>Conversor de medidas</h1>
			</div>-->

			<div id="medidas_cuerpo">
				<p id="medidas_cuerpo_titulo">Seleccióne una unidad para convertir:</p>
                <span id='volumen_sel' class="p_sel"><input type="checkbox" class="form-check-input" id="cbox1" value="5"> Volumen</span>
                <span id='longitud_sel' class="p_sel"><input type="checkbox" class="form-check-input" id="cbox2" value="5"> Longitud</span>
                <span id='temperatura_sel' class="p_sel"><input type="checkbox" class="form-check-input" id="cbox3" value="5"> Temperatura</span>
                <span id='peso_sel' class="p_sel"><input type="checkbox" class="form-check-input" id="cbox4" value="5"> Peso</span>
                <span id='presion_sel' class="p_sel"><input type="checkbox" class="form-check-input" id="cbox5" value="5"> Presión</span>
                
                
				<input type="text" id="medidas_cuerpo_numero" placeholder="Número aquí">
                <div id="medidas_cuerpo_medio">
                    <span id="medidas_cuerpo_medida">Seleccione una medida:</span>
                    <select id="medidas_cuerpo_top">
                        <option value="-----">-----</option>
                    </select><br><br>
                </div>
                
                <div id="medidas_base">
                    <span id="medidas_base_convertir">Seleccione la medida a convertir:</span>
                    <select id="medidas_base_bot">
                        <option value="-----">-----</option>
                    </select>
                </div>
                <div id="medidas_boton">
                    <a id="medidas_boton_convertir"><i class="fa fa-exchange" aria-hidden="true"></i> <b>Convertir</b></a>
                </div>
                <div id="medidas_resultado">
				    <span id="medidas_resultado_tras">=</span>
                    <span id="medidas_resultado_numero"></span>
                    <span id="medidas_resultado_medida"></span>
			    </div>
			</div>
		</div>
		
		<div id="general" class="0">
        
		    <div id="digital">
		        <div id="digi1">
		        	<span id="tabulador">0</span>
		            <span id="digiz"></span>
		            <span id="digde"></span>
		        </div>
		        <div id="digi2">
		            
		        </div>
		    </div>
		    
		    <div id="botones">
		        <div id="fila1" class="fila">
		            <a id="potencia" class="boton"><i class="fa fa-superscript"></i></a>
		            <a id="raiz" class="boton">√</a>
		            <a id="porcentaje" class="boton">%</a>
		            <a id="fraccion" class="boton">x/y</a>
		            <a id="division" class="boton">÷</a>
		        </div>
		        
		        <div id="fila2" class="fila">
		            <a id="log" class="boton">log</a>
		            <a id="siete" class="boton" name="numero">7</a>
		            <a id="ocho" class="boton" name="numero">8</a>
		            <a id="nueve" class="boton" name="numero">9</a>
		            <a id="por" class="boton">x</a>
		        </div>
		        
		        <div id="fila3" class="fila">
		            <a id="pi" class="boton">π</a>
		            <a id="cuatro" class="boton" name="numero">4</a>
		            <a id="cinco" class="boton" name="numero">5</a>
		            <a id="seis" class="boton" name="numero">6</a>
		            <a id="menos" class="boton">-</a>
		        </div>
		        
		        <div id="fila4" class="fila">
		            <a id="euler" class="boton">e</a>
		            <a id="uno" class="boton" name="numero">1</a>
		            <a id="dos" class="boton" name="numero">2</a>
		            <a id="tres" class="boton" name="numero">3</a>
		            <a id="mas" class="boton">+</a>
		        </div>
		        
		        <div id="fila5" class="fila">
		            <a id="pariz" class="boton">(</a>
		            <a id="parde" class="boton">)</a>
		            <a id="cero" class="boton" name="numero">0</a>
		            <a id="coma" class="boton">,</a>
		            <a id="igual" class="boton" name="igual">=</a>
		        </div>

		        <div id="fila6" class="fila">
		        	<a id="borrar" class="boton"><i class="fa fa-eraser"></i></a>
		        </div>
		    </div>
		    
		</div>
		
		<div id="historial" class="0">
             <h2>Historial</h2>

             <div id="historial_cuerpo">
             	<table id="historial_tabla">
             		<tr id="historial_tabla_menu">
             			<th id="th1"><b>Operacíon</b></th>
             			<th id="th2"><b>Resultado</b></th>
             		</tr>
             		<tbody id="historial_tabla_cuerpo">
             			
             		</tbody>
             	</table>
             </div>
		</div>

		<div id="aux">
		    <span id="digiz0"></span>
		</div>


        <script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/slick.min.js"></script>
		<script src="assets/js/nouislider.min.js"></script>
		<script src="assets/js/jquery.zoom.min.js"></script>
		<script src="assets/js/programa21.js"></script>
		<script src="assets/js/jquery.js" type="text/javascript"></script>

	</body>
</html>
